/*
 * Copyright 2021 The happyProxy Project
 *
 * The happyProxy Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package com.sinosoft.lingkang.happyproxy.constants;

/**
 * @Author 绫小路
 * @Date 2021/2/22
 */
public class HappyProxyConstants {

  public static final String HTTP_REQUEST_DECODER = "httpRequestDecoder";

  public static final String HTTP_PROXY_AUTHORIZATION_HEADER = "Proxy-Authorization";

  //proxy authorization fail return body
  public static final byte[] PROXY_AUTHORIZATION_FAIL_RESPONSE = ("HTTP/1.1 407 Proxy Authentication Required\n"
      + "Proxy-Authenticate: Basic realm=\"Restricted Files\"\n"
      + "content-type: text/html;charset=utf-8\n\n").getBytes();

  public static void main(String[] args) {
    String re="<html><body>禁止访问lingkang.top！<script>alert('嘿嘿!')</script></body></html>";
    System.out.println(re.getBytes().length);
  }
}
