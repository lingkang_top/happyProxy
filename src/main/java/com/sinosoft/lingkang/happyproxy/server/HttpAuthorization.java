/*
 * Copyright 2021 The happyProxy Project
 *
 * The happyProxy Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package com.sinosoft.lingkang.happyproxy.server;

/**
 * @Author 绫小路
 * @Date 2021/2/22
 */
public interface HttpAuthorization {

  /**
   * 动态认证，每个请求都会认证一次
   * <h2>example</h2>
   * <pre>
   * public boolean authorization(String username, String password) {
   *   if ("123".equals(username)){
   *     if ("123456".equals(password)){
   *       return true;
   *     }
   *   }
   *   return false;
   * }
   * </pre>
   *
   * @param username
   * @param password
   * @return
   */
  public boolean authorization(String username, String password);

}
