/*
 * Copyright 2021 The happyProxy Project
 *
 * The happyProxy Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package com.sinosoft.lingkang.happyproxy.server;

import com.sinosoft.lingkang.happyproxy.constants.HappyProxyConstants;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpRequestDecoder;

/**
 * @Author 绫小路
 * @Date 2021/2/22
 */
public class DefaultProxyServerInitializer extends ChannelInitializer<SocketChannel> {

  private HttpAuthorization authorization;
  private HttpProxyServerRequestFilter requestFilter;
  private HttpProxyServerFlowCount httpProxyServerFlowCount;


  public DefaultProxyServerInitializer setHttpProxyServerFlowCount(HttpProxyServerFlowCount httpProxyServerFlowCount) {
    this.httpProxyServerFlowCount = httpProxyServerFlowCount;
    return this;
  }

  protected DefaultProxyServerInitializer setAuthorization(HttpAuthorization authorization) {
    this.authorization = authorization;
    return this;
  }

  protected DefaultProxyServerInitializer setRequestFilter(HttpProxyServerRequestFilter requestFilter) {
    this.requestFilter = requestFilter;
    return this;
  }


  protected void initChannel(SocketChannel socketChannel) throws Exception {
    socketChannel.pipeline().addLast(HappyProxyConstants.HTTP_REQUEST_DECODER, new HttpRequestDecoder());
    socketChannel.pipeline().addLast(
        new ConnectToRemoteServerHandler()
            .setAuthorization(authorization)
            .setRequestFilter(requestFilter)
            .setHttpProxyServerFlowCount(httpProxyServerFlowCount)
    );
  }
}
