/*
 * Copyright 2021 The happyProxy Project
 *
 * The happyProxy Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package com.sinosoft.lingkang.happyproxy.server;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.HttpRequest;

/**
 * @Author 绫小路
 * @Date 2021/2/22
 */
public interface HttpProxyServerRequestFilter {

  /**
   * 自定义请求过滤 return true 表示放行 return false 过滤掉无法再访问
   * <h2>example</h2>
   * <pre>
   *           public boolean doFilter(HttpRequest request, Channel inputChannel, String host, int port) {
   *             if ("lingkang.top".equals(host)) {
   *               String response = "HTTP/1.1 200 OK\n"
   *                   + "content-type: text/html;charset=utf-8\n"
   *                   + "content-length: 86\n\n"
   *                   + "<html><body>禁止访问lingkang.top！<script>alert('嘿嘿!')</script></body></html>";
   *               ByteBuf byteBuf = null;
   *               try {
   *                 byteBuf = Unpooled.wrappedBuffer(response.getBytes("UTF-8"));
   *               } catch (UnsupportedEncodingException e) {
   *                 e.printStackTrace();
   *               }
   *               inputChannel.writeAndFlush(byteBuf);
   *               return false;
   *             }
   *             return true;
   *           }
   * </pre>
   *
   * @param request
   * @param inputChannel
   * @param host
   * @param port
   * @return
   */
  public boolean doFilter(HttpRequest request, Channel inputChannel, String host, int port);

}
