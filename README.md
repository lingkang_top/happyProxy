# happyProxy
一个基于netty实现代理服务器 proxyServer<br>
基于Netty事件的网络库之上用Java编写的高性能HTTP代理Server。它非常稳定，性能良好，并且易于集成到您的项目中。<br>
> 创作源自生活，我经历在内网环境进行工作，所以业余时间使用java实现一个代理服务器。<br>
内网环境工作虽然有proxy让我上网，但是 bilibili.com 无法访问，快乐不起来，故项目取名 happyProxy
您可使用happyProxy尽情创作！say:2021-02-23

## 功能
* 请求过滤
* 代理认证
* 流量统计
## 计划的功能
* 响应过滤
* 流量限速
## 启动
前往下载 [jar 包](http://https://gitee.com/lingkang_top/happyProxy/releases/1.0.1-SNAPSHOT)
引入
```text
  public static void main(String[] args) {
    new DefaultHappyProxyServer()
        .setPort(8888)
        .start();
  }
```
#### 1.实现过滤

```java
  //在主机 192.168.31.62 上运行此代码
  public static void main(String[] args) {
    new DefaultHappyProxyServer()
        .setPort(8888)
        .setAuthorization(new HttpAuthorization() {
          @Override
          public boolean authorization(String username, String password) {
            return true;
          }
        })
        .setRequestFilter(new HttpProxyServerRequestFilter() {
          @Override
          public boolean doFilter(HttpRequest request, Channel inputChannel, String host, int port) {
            if ("lingkang.top".equals(host)) {
              String response = "HTTP/1.1 200 OK\n"
                  + "content-type: text/html;charset=utf-8\n"
                  + "content-length: 86\n\n"
                  + "<html><body>禁止访问lingkang.top！<script>alert('嘿嘿!')</script></body></html>";
              ByteBuf byteBuf = null;
              try {
                byteBuf = Unpooled.wrappedBuffer(response.getBytes("UTF-8"));
              } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
              }
              inputChannel.writeAndFlush(byteBuf);
              return false;
            }
            return true;
          }
        })
        .start();
  }
```
![输入图片说明](https://images.gitee.com/uploads/images/2021/0224/005506_36b3ce3a_2159235.png "屏幕截图.png")

#### 2.认证

```
  //在主机 192.168.31.62 上运行此代码
  public static void main(String[] args) {
    new DefaultHappyProxyServer()
        .setPort(8888)
        .setAuthorization(new HttpAuthorization() {
          @Override
          public boolean authorization(String username, String password) {
            if ("123456".equals(username)) {
              return true;
            }
            return false;
          }
        })
        .start();
  }
```
![输入图片说明](https://images.gitee.com/uploads/images/2021/0224/005826_293b9545_2159235.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0224/005917_1eaac060_2159235.png "屏幕截图.png")
